################################################
Cross-compiling Qt for Raspberry Pi with Ansible
################################################

I use a CentOS 7 Linux VM to develop Qt 5-based software for a Raspberry Pi 2.  However, the process is error-prone so I have written some Ansible playbooks to turn it into a set of well-defined tasks.  These playbooks were tested with Ansible 2.3.1.0 on a linux desktop running CentOS 7 (release 1611) and the Pi running a clean install of the Pi Foundation's Debian Jessie, release 2017-04-10.  These playbooks are based on the excellent wiki guide at https://wiki.qt.io/RaspberryPi2EGLFS.

Ansible is available from the `EPEL repo <https://fedoraproject.org/wiki/EPEL>`_.  Enable EPEL with::

   sudo rpm -i https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

And install Ansible with::

   sudo yum install ansible   



***********
Assumptions
***********

You should be able SSH from the CentOS 7 computer to the Raspberry Pi.  Where applicable, I'll use ``centos7`` as the hostname for the CentOS7 computer, and ``pi`` for the Raspberry Pi.



**********************
Set up Your Hosts File
**********************

#) Get the IP address of the Pi.

#) On CentOS 7, add an entry to ``/etc/hosts`` with the Pi's IP address and hostname, for example::

    192.168.1.3 pi


************************************
Set up SSH Keys Before Using Ansible
************************************

#) Make sure SSH is enabled on both CentOS 7 and the Pi.

#) On both CentOS 7 and the Pi, create a ``.ssh`` folder in your home folder if it
   doesn't exist already::

    mkdir ~/.ssh && chmod go-rwx ~/.ssh

#) On CentOS 7, create an SSH key so that Ansible can configure the system without
   asking for SSH credentials::

    # When prompted for a passphrase, leave it blank
    ssh-keygen -f ~/.ssh/localhost &&
    cat ~/.ssh/localhost.pub >> ~/.ssh/authorized_keys &&
    chmod go-rw ~/.ssh/authorized_keys

#) On CentOS 7, create *another* SSH key that you'll use for the Pi.  Strictly speaking, you could use the same key that you created in the previous step, but I prefer to have multiple keys::

    # When prompted for a passphrase, leave it blank
    ssh-keygen -f ~/.ssh/remotepi &&
    cat ~/.ssh/remotepi.pub | ssh pi@pi "cat >> ~/.ssh/authorized_keys && chmod go-rw ~/.ssh/authorized_keys"

#) On CentOS 7, configure SSH to automatically use the ``remotepi`` key for the Pi::

    echo -e "Host pi\n\tHostname pi\n\tUser pi\n\tIdentityFile ~/.ssh/remotepi\n" >> ~/.ssh/config &&
    chmod go-rw ~/.ssh/config



*************
The Playbooks
*************

The playbooks are listed here in the order that they should be run.


01_pi_setup_playbook.yml
========================

This playbook sets up a Pi.  Run this playbook with the ``--ask-become`` option so that Ansible can ask for your Pi's sudo password.  The command will look like::

    ansible-playbook 01_pi_setup_playbook.yml --ask-become -i inventory.txt

Playbook highlights include:

* Enabling Raspbian source repos.

* Installing dependencies required for building Qt


02_dev_setup_playbook.yml
=========================

This playbook sets up a CentOS 7 development environment.  Before you start:

#) Make sure the Pi is online and accessible from the dev computer via the ``pi`` hostname.  Use ``ssh pi`` to confirm everything is working.  The Pi must be accessible because this playbook will copy files from the Pi to the dev computer.

#) Make sure you've already run the Pi setup playbook (``01_pi_setup_playbook.yml``), or you'll be missing required files.

#) Run this playbook with the ``--ask-become`` option so that you can supply Ansible with your sudo password.  The command line will look like::

    ansible-playbook 02_dev_setup_playbook.yml --ask-become -i inventory.txt

**More Details**: Highlights from this playbook include:

* Installing required packages.
* Creating a ~/raspi folder to contain the cross-compilation software.
  This includes copying required files from an actual Pi, which is why a
  Pi must be accessible from the dev computer.
* Compiling Qt 5.9 for the Raspberry Pi.


03_deploy_to_pi_playbook.yml
============================

This playbook deploys Qt to the Pi.  You can customize it to deploy your software to the Pi too.

The command line will look like::

    ansible-playbook 03_deploy_to_pi_playbook.yml --ask-become -i inventory.txt



****
Test
****

After the playbooks have successfully run we can try to cross-compile one of Qt's sample applications.  Assuming you used ~/rpi as the ``pi_folder`` in the Ansible playbooks, run the following commands::

    cd ~/rpi/qtsrc/qtquickcontrols2/examples/ &&
    ~/rpi/qt5/bin/qmake &&
    make &&
    scp quickcontrols2/gallery/gallery pi:/home/pi

Now log in to your Pi and run the /home/pi/gallery.  You should be presented with the Qt QuickControls 2 demo gallery.



****
More
****
I posted a little about this at `my website <https://woltman.com/blog/cross-compile-qt-on-centos-7-for-a-raspberry-pi/>`_.
